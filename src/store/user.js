/*
 * @Author: loserjohn
 * @Date: 2022-04-04 22:07:18
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-11 00:07:12
 * @Description: ---
 * @FilePath: \vite_ts\src\store\user.ts
 */


import { defineStore } from 'pinia'


export const useUser = defineStore('user', {
    state: () => {
        return {
            name: '哈哈',
            age: 12,
            level: 1
        }
    },
    getters: {
        nick_name(state) {
            return state.name + '123'
        }
    },
    actions: {
        changeName() {

            this.name = '我变化了'
        }
    },

    // 开启数据缓存
    persist: {
        enabled: true,
        strategies: [
            {
                key: 'test',
                storage: localStorage,
                //   paths: ['name'],
            }
        ]
    }
})