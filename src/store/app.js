 
/*
 * @Author: loserjohn
 * @Date: 2022-04-04 22:07:18
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-04-11 12:13:38
 * @Description: ---
 * @FilePath: \vite_ts\src\store\app.ts
 */


import { defineStore } from 'pinia'


export const useApp = defineStore('app', {
    state: () => {
        return {
            token: '', 
        }
    },
    getters: {
        useToken(state) {
            return state.token 
        }
    },
    actions: {
        setToken(data) {
            // debugger
            this.token = data
            
        }
    },

    // 开启数据缓存
    persist: {
        enabled: true,
        strategies: [
            {
                key: 'app',
                storage: sessionStorage,
                //   paths: ['name'],
            }
        ]
    }
})