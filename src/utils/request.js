
/*
 * @Descripttion:
 * @version:
 * @ Modified by: loserjohn
 * @ Modified time: 2021-12-01 21:31:52
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-05-08 23:03:45
 */
import { useApp } from '@/store/app'
import axios from 'axios'
// import { Dialog, Toast } from 'vant'

let loadingInstance = null
let requstQueue = 0
console.log(import.meta.env)


// create an axios instance
const service = axios.create({
  baseURL: import.meta.env.VITE_BASE_API, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
})



// request interceptor
service.interceptors.request.use(
  async (config) => {
    const store = useApp()
    console.log(store)
    if (store.token) {
      config.headers['Authorization'] = 'Bearer ' + store.token
    }
    showLoading()
    return config
  },
  error => {
    // do something with request error

    hideLoading()
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(

  response => {
    const res = response.data
    // debugger
    hideLoading()
    if (response.status >= 200 && response.status < 300) {
      return res
    } else {

      // Notify({
      //   message: JSON.stringify(res),
      //   type:'error'
      // });
      // Dialog.alert({
      //   title: '出错拉',
      //   message: JSON.stringify(res),
      //   theme: 'round-button',
      // }).then(() => {
      //   // on close
      // });
      // return res
      return Promise.reject(res)
    }
    // if (res.status ===  401) {

    // }else{

    // }
  },
  (error) => {
    hideLoading();

    console.log('err：', error.response) // for debug
    const err = error.response.data || null

    // Notify({
    //   message:  err && err.message ? err.message : err.request_url,
    //   type:'error'
    // });
    // Dialog.alert({
    //   title: '出错拉',
    //   message: err && err.message ? err.message : err.request_url,
    //   theme: 'round-button',
    // }).then(() => {
    //   // on close
    // });
    return Promise.reject(err)
  }
)

function showLoading() {

  if (requstQueue <= 0) {
    // console.log('loading', requstQueue)
    // loadingInstance = Toast.loading({
    //   duration: 0,
    //   forbidClick: true,
    //   message: '请稍后',
    // });
    requstQueue = 0
  }
  requstQueue += 1
}

function hideLoading() {
  requstQueue -= 1
  if (requstQueue <= 0) {
    // console.log('hideLoading', requstQueue)
    Toast.clear();;
    loadingInstance = null;
    requstQueue = 0
  }

}

export default service
