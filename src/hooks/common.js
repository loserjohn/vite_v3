/*
 * @Author: loserjohn
 * @Date: 2022-04-03 22:39:00
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-10 19:35:37
 * @Description: ---
 * @FilePath     : \vite_v3\src\hooks\common.js
 */
// import { dicts } from '@/apis/app';
import { onMounted, reactive, ref, inject} from 'vue';
import { useRouter, useRoute} from 'vue-router';

/**
 *
 *
 * @export  common  hook
 * @return {*}
 */
export default function () {

  const router = useRouter();
  const route = useRoute();
  const global = reactive({
    dict: {}
  });

  const nav = (url, data) => {
    router.push({ path: url, query: data });
  };
  const back = () => {
    router.go(-1);
  };
  const ifMobile = inject('ifMobile');


  onMounted(() => {


  });

  return {
    back,
    nav,
    router,
    route,
    global,
    ifMobile
  };
}
