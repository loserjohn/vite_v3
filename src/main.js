/*
 * @Author: loserjohn
 * @Date: 2022-04-04 21:08:31
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-09 10:51:30
 * @Description: ---
 * @FilePath     : \vite_v3\src\main.js
 */
import '@/styles/custom-theme.css';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import 'hover.css'
import { createPinia } from 'pinia';
import piniaPluginPersist from 'pinia-plugin-persist';
import { createApp } from 'vue';
// import vue3videoPlay from 'vue3-video-play'; // 引入组件
// import 'vue3-video-play/dist/style.css'; // 引入css
import App from './App.vue';
import router from './router/index';






const store = createPinia()
store.use(piniaPluginPersist)
createApp(App).use(router).use(store).use(ElementPlus).mount('#app')