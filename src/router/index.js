/*
 * @Author: loserjohn
 * @Date: 2022-04-01 10:18:02
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-10 15:20:37
 * @Description: ---
 * @FilePath     : \vite_v3\src\router\index.js
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/home/index.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/business',
    name: 'Business',
    component: ()=>import('../views/business/index.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
    component: ()=>import('../views/contact/index.vue')
  },
  {
    path: '/join',
    name: 'Join',
    component: ()=>import('../views/join/index.vue')
  },
  {
    path: '/joinDetail/:id',
    name: 'JoinDetail',
    component: ()=>import('../views/joinDetail/index.vue')
  },
  {
    path: '/news',
    name: 'News',
    component: ()=>import('../views/news/index.vue')
  },
  {
    path: '/newsDetail/:id',
    name: 'NewsDetail',
    component: ()=>import('../views/newsDetail/index.vue')
  },


]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
