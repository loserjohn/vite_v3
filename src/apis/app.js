
/*
 * @Author: loserjohn
 * @Date: 2022-04-10 12:11:09
 * @LastEditors: loserjohn
 * @LastEditTime: 2022-05-08 22:55:18
 * @Description: ---
 * @FilePath: \my-vue-app\src\apis\app.js
 */
import request from '@/utils/request.js'


/**
 *获取 字典项
 *
 * @interface productList_params
 */

export const dicts = () => {

    return request({
        url: '/api/dicts',
        methods: 'GET',
    })
}


/**
 *获取 登录
 *
 * @interface productList_params
 */

export const userLogin = (data) => {

    return request({
        url: '/api/home/login',
        methods: 'GET',
        params: data
    })
}