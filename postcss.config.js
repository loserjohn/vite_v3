/*
 * @Author       : xh
 * @Date         : 2022-05-09 09:36:51
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-11 14:16:06
 * @FilePath     : \vite_v3\postcss.config.js
 */
module.exports = {
  'plugins': {
    'postcss-pxtorem': {
      rootValue: 16, // 根节点字体大小，以100px为例，可根据自己需求修改75px = 1rem
      propList: ['*'],
      selectorBlackList: ['.px']
      // 过滤掉.px-开头的class，不进行rem转换
    }
  }
};
