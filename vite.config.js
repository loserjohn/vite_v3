/*
 * @Author: loserjohn
 * @Date: 2022-04-05 09:37:35
 * @LastEditors  : xh
 * @LastEditTime : 2022-05-10 14:33:30
 * @Description: ---
 * @FilePath     : \vite_v3\vite.config.js
 */
import vue from '@vitejs/plugin-vue';
import { defineConfig } from 'vite';
import styleImport, { VantResolve } from 'vite-plugin-style-import';
const { resolve } = require('path');
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), styleImport({
    resolves: [VantResolve()]
  })],
  build: {
    // assetsDir:'static',
    assetsInlineLimit: 6144,
    // cssCodeSplit:false,
    reportCompressedSize: false
  },
  preview: {
    port: 3000
  },
  // base: '/wap/',
  server: {
    open: true,
    port: 3030,
    host: '0.0.0.0'
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      '~': resolve(__dirname, './public')
    },
    extensions: ['.ts', '.js', '.vue', '.css', '.scss']
  }
});
